`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    07:36:01 11/16/2017 
// Design Name: 
// Module Name:    led_test 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module led_test(
    input system_clk_p,
    input system_clk_n,
	 output led_0,
	 output led_1,
	 output led_2
    );
	 
IBUFDS clkingen (
.I(system_clk_p),
.IB(system_clk_n),
.O(clk));
reg one;
assign led_0=one;
assign led_1=one;
assign led_2=one;
always @ (posedge clk)
begin
	one <= 1'd1;
end	

endmodule
